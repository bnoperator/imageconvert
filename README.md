#Intro
A tool to convert the production QC images into a standard PS96 format in order to view it with BioNavigator

#Getting started
Get latest code:
```
git clone https://fnaji@bitbucket.org/bnoperator/imageconvert.git
```

* run imageConverterTool.bat file
* select the folder you wish to be converted
* a new folder will be created with the same name but with an additional suffix of "-ImagesResult"
* a message indicating completion will pop up.


The new folder can now be viewed with the [Image Overview Step] in BioNavigator


#Requirements
The .bat file assumes you have an R installed at:

```
C:\PamSoft\R\R-2.15.3
```
edit it if you have R located elsewhere.